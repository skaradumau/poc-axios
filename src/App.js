import logo from './logo.svg';
import './App.css';

import { useState } from 'react';
import axios from 'axios';

const headers = {
  foooooooooo: 'foooooooooooo',
  baaaaaaaaaar: 'baaaaaaaaaar'
};

const instance = axios.create({
  headers: headers
});

function App() {
  const [value, setValue] = useState('');

  const setCookie = (_value) => {
    return instance.interceptors.request.use(
      (config) => {
        config.headers.baaaaaaaaaar = _value;
    
        return config;
      },
      (error) => console.log({error}));
  }

  const sendRequest = () => {
    return instance.get('https://httpbin.org/get').then(res => console.log(res))
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <div style={{ display: 'flex', gap: '50px' }}>
          <input value={value} onChange={(e) => setValue(e?.target?.value)} />
          <button onClick={() => setCookie(value)}>Set cookie</button>
          <button onClick={() => setCookie('zooooooooooo')}>Reset headers</button>
          <button onClick={sendRequest}>Send request</button>
        </div>
      </header>
    </div>
  );
}

export default App;
